﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Xml.Linq;

namespace Manhattan.Conversions
{
    public class Currency : Value
    {
        private const string BaseCurrency = "EUR";
        private static readonly Dictionary<string, double> CurrenciesWithValues = new Dictionary<string, double>();
        private const string ConversionFileName = "Conversions.xml";

        public override IEnumerable<string> GetAll()
        {
            return CurrenciesWithValues.Keys;
        }

        public Currency()
        {
            if (CurrenciesWithValues.Count != 0) return;
            
            try
            {
                ReadConversionFromStorage();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            };
        }

        public override double Convert(string from, string to, double value)
        {
            if (from == BaseCurrency)
            {
                return value * CurrenciesWithValues[to];
            }

            if (to == BaseCurrency)
            {
                return value / CurrenciesWithValues[from];
            }

            return value / CurrenciesWithValues[from] * CurrenciesWithValues[to];
        }

        public static async void FetchValues()
        {
            using var client = new HttpClient
            {
                BaseAddress = new Uri("https://api.exchangeratesapi.io/")
            };
            var response = await client.GetAsync("latest");
            if (!response.IsSuccessStatusCode) return;
            GetAvailableCurrencies();
            var result = JsonDocument.Parse(response.Content.ReadAsStringAsync().Result);
            var root = result.RootElement;
            var rates = root.GetProperty("rates");
            foreach (var currency in GetAvailableCurrencies())
            {
                CurrenciesWithValues.Add(currency, rates.GetProperty(currency).GetDouble());
            }
            SaveValues();
        }

        private static string[] GetAvailableCurrencies()
        {
            var doc = XDocument.Load("AvailableCurrencies.xml");
            var rootElement = doc.Root;
            if (rootElement == null) throw new Exception("Document with available currencies is not present.");
            var count = (int) rootElement.Attribute("count");
            var currencies = new string[count];
            var loadedCurrencies = rootElement.Elements();
            var i = 0;
            foreach (var element in loadedCurrencies)
            {
                currencies[i] = element.Value;
                i++;
            }

            return currencies;
        }

        private static void SaveValues()
        {
            new XDocument(
                new XElement("Conversions",
                    new XAttribute("timestamp", DateTime.Now.ToString(CultureInfo.InvariantCulture)),
                    CurrenciesWithValues.Select(currencyWithValue => new XElement("Conversion",
                        new XAttribute("currency", currencyWithValue.Key),
                        new XAttribute("value", currencyWithValue.Value)
                    ))
                )
            ).Save(ConversionFileName);
        }

        private static void ReadConversionFromStorage()
        {
            var doc = XDocument.Load(ConversionFileName);
            if (doc.Root == null) throw new Exception("Conversions file does not exist, or is invalid.");
            var conversions = doc.Root.Elements();
            foreach (var element in conversions)
            {
                var currencyAttribute = element.Attribute("currency");
                var valueAttribute = element.Attribute("value");
                if (currencyAttribute == null || valueAttribute == null) throw new Exception("Conversions file is invalid. Remove or modify it.");
                CurrenciesWithValues.Add(currencyAttribute.Value, System.Convert.ToDouble(currencyAttribute.Value));
            }
        }
    }
}