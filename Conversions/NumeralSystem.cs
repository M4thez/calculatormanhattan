﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Manhattan.Conversions
{
    internal class NumeralSystemsConversion
    {
		/// <summary>
		/// Decimal to binary
		/// </summary>
		internal static string DecToBin(string x) => Convert.ToInt32(x, 2).ToString();

		/// <summary>
		/// Decimal to octadecimal
		/// </summary>
		internal static string DecToOcta(string x) => Convert.ToInt32(x, 8).ToString();

		/// <summary>
		/// Decimal to hexadecimal
		/// </summary>
		internal static string DecToHex(string x) => Convert.ToInt32(x, 16).ToString();

		/// <summary>
		/// Binary to decimal
		/// </summary>
		internal static string BinToDec(string x) => Convert.ToInt32(x, 2).ToString();

		/// <summary>
		/// Binary to octadecimal
		/// </summary>
		internal static string BinToOcta(string x) => Convert.ToString(Convert.ToInt32(x, 2), 8);

		/// <summary>
		/// Binary to hexadecimal
		/// </summary>
		internal static string BinToHex(string x) => Convert.ToString(Convert.ToInt32(x, 2), 16);

		/// <summary>
		/// Octadecimal to binary
		/// </summary>
		internal static string OctaToBin(string x) => Convert.ToString(Convert.ToInt32(x, 8), 2);

		/// <summary>
		/// Octadecimal to decimal
		/// </summary>
		internal static string OctaToDec(string x) => Convert.ToString(Convert.ToInt32(x, 8), 10);

		/// <summary>
		/// Octadecimal to hexadecimal
		/// </summary>
		internal static string OctaToHex(string x) => Convert.ToString(Convert.ToInt32(x, 8), 16);

		/// <summary>
		/// Hexadecimal to binary
		/// </summary>
		internal static string HexToBin(string x) => Convert.ToString(Convert.ToInt32(x, 16), 2);

		/// <summary>
		/// Hexadecimal to octadecimal
		/// </summary>
		internal static string HexToOcta(string x) => Convert.ToString(Convert.ToInt32(x, 16), 8);

		/// <summary>
		/// Hexadecimal to decimal
		/// </summary>
		internal static string HexToDec(string x) => Convert.ToString(Convert.ToInt32(x, 16), 10);
	}
}
