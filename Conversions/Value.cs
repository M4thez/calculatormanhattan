﻿using System.Collections.Generic;

namespace Manhattan.Conversions
{
    public abstract class Value
    {
        public abstract IEnumerable<string> GetAll();
        public abstract double Convert(string from, string to , double value);
    }
}