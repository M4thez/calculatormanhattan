﻿using System.Windows;

namespace Manhattan
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void BasicCalc_Click(object sender, RoutedEventArgs e)
        {
            NavigationService?.Navigate(new BasicCalculator());
        }

        private void AdvCalc_Click(object sender, RoutedEventArgs e)
        {
            NavigationService?.Navigate(new AdvancedCalculator());
        }

        private void UnitConv_Click(object sender, RoutedEventArgs e)
        {
            NavigationService?.Navigate(new UnitConverter());
        }
    }
}
