﻿using System.Windows.Navigation;

namespace Manhattan
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : NavigationWindow
    {
        public MainWindow()
        {
            Conversions.Currency.FetchValues();
            InitializeComponent();
        }
    }
}