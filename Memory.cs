﻿namespace Manhattan
{
    public class Memory : IMemory<double>
    {
        private double _value;

        public double Read() => _value;

        public void Set(double value)
        {
            _value = value;
        }

        public void Add(double value)
        {
            _value += value;
        }

        public void Sub(double value)
        {
            _value -= value;
        }

        public void Clear()
        {
            _value = 0;
        }
    }
}