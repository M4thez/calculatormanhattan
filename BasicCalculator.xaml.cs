﻿using System;
using System.Globalization;
using System.Windows;
using Manhattan.Math;

namespace Manhattan
{
    /// <summary>
    /// Interaction logic for BasicCalculator.xaml
    /// </summary>
    public partial class BasicCalculator
    {
        private delegate double CalcAction(double a, double b);

        private IMemory<double> Memory => _memory ??= new Memory();

        private IMemory<double> _memory;
        private CalcAction _actionToPerform;
        private double _prevInput;
        private bool _operationComplete;

        private double CurrentInput
        {
            get
            {
                try
                {
                    return double.Parse(TxtDisplay.Text, CultureInfo.InvariantCulture);
                }
                catch (FormatException)
                {
                    ResetAll("Invalid input.");
                    return 0;
                }
            }
        }

        public BasicCalculator()
        {
            InitializeComponent();
        }

        private void Btn0_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(0);
        }

        private void Btn1_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(1);
        }

        private void Btn2_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(2);
        }

        private void Btn3_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(3);
        }

        private void Btn4_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(4);
        }

        private void Btn5_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(5);
        }

        private void Btn6_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(6);
        }

        private void Btn7_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(7);
        }

        private void Btn8_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(8);
        }

        private void Btn9_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(9);
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            SetOperation(" + ", BasicOperations.Add);
        }

        private void BtnSub_Click(object sender, RoutedEventArgs e)
        {
            SetOperation(" - ", BasicOperations.Sub);
        }

        private void BtnMul_Click(object sender, RoutedEventArgs e)
        {
            SetOperation(" × ", BasicOperations.Mul);
        }

        private void BtnDiv_Click(object sender, RoutedEventArgs e)
        {
            SetOperation(" ÷ ", BasicOperations.Div);
        }

        private void BtnPct_Click(object sender, RoutedEventArgs e)
        {
            SetOperation(" % ", BasicOperations.Pct);
        }

        private void BtnPow_Click(object sender, RoutedEventArgs e)
        {
            SetOperation(" ^ ", BasicOperations.Pow);
        }

        private void BtnDiv1Byx_Click(object sender, RoutedEventArgs e)
        {
            DisplayPrev($"1 / {CurrentInput} = ");
            Display(BasicOperations.Div(1, CurrentInput));
        }

        private void BtnSqrt_Click(object sender, RoutedEventArgs e)
        {
            DisplayPrev($"√({CurrentInput}) = ");
            Display(BasicOperations.Sqrt(CurrentInput));
        }

        private void BtnPoint_Click(object sender, RoutedEventArgs e)
        {
            if (!TxtDisplay.Text.Contains('.')) AddToDisplay(".");
        }

        private void BtnNeg_Click(object sender, RoutedEventArgs e)
        {
            Display($"-{TxtDisplay.Text}");
        }

        private void BtnEnter_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddToDisplayPrev($"{TxtDisplay.Text} = ");
                if (_actionToPerform != null)
                    Display(_actionToPerform(_prevInput, CurrentInput));
                ResetOperation();
                EndOperation();
            }
            catch (ArithmeticException)
            {
                Display("ERROR!");
            }
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            ResetAll();
        }

        private void BtnCE_Click(object sender, RoutedEventArgs e)
        {
            ResetOperation();
            ClearDisplay();
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            int length = TxtDisplay.Text.Length;
            if (length < 2) ClearDisplay();
            else Display(TxtDisplay.Text.Substring(0, length - 1));
        }

        private void BtnMClear_Click(object sender, RoutedEventArgs e)
        {
            Memory.Clear();
        }

        private void BtnMAdd_Click(object sender, RoutedEventArgs e)
        {
            Memory.Add(CurrentInput);
        }

        private void BtnMSub_Click(object sender, RoutedEventArgs e)
        {
            Memory.Sub(CurrentInput);
        }

        private void BtnMSet_Click(object sender, RoutedEventArgs e)
        {
            Memory.Set(CurrentInput);
        }

        private void BtnMRead_Click(object sender, RoutedEventArgs e)
        {
            Display(Memory.Read());
        }

        private void Display(double value)
        {
            TxtDisplay.Text = value.ToString(CultureInfo.InvariantCulture);
        }

        private void Display(string value)
        {
            TxtDisplay.Text = value;
        }

        private void DisplayPrev(string value)
        {
            TxtDisplayPrev.Text = value;
        }

        private void AddToDisplay(int value)
        {
            if (_operationComplete)
            {
                Display(value);
                StartOperation();
            }    
            else if (TxtDisplay.Text == "0")
            {
                Display(value);
            }
            else AddToDisplay(value.ToString(CultureInfo.InvariantCulture));
        }

        private void AddToDisplay(string value)
        {
            TxtDisplay.Text += value;
        }

        private void AddToDisplayPrev(string value)
        {
            TxtDisplayPrev.Text += value;
        }

        private void ClearDisplay()
        {
            TxtDisplay.Text = "0";
        }

        private void ClearPreview()
        {
            TxtDisplayPrev.Text = "0";
        }

        private void SavePrevValue()
        {
            try
            {
                _prevInput = double.Parse(TxtDisplay.Text, CultureInfo.InvariantCulture);
                DisplayPrev(TxtDisplay.Text);
                ClearDisplay();
            }
            catch (FormatException)
            {
                ResetAll("Invalid input.");
            }
        }

        private void SetOperation(string sign, CalcAction operation)
        {
            if (_actionToPerform == null) SavePrevValue();
            else TxtDisplayPrev.Text = TxtDisplayPrev.Text.Substring(0, TxtDisplayPrev.Text.Length - 3);
            AddToDisplayPrev(sign);
            _actionToPerform = operation;
        }

        private void ResetAll()
        {
            ResetOperation();
            ClearDisplay();
            ClearPreview();
        }

        private void ResetAll(string message)
        {
            ResetOperation();
            Display(message);
            ClearPreview();
        }

        private void ResetOperation()
        {
            _prevInput = 0;
            _actionToPerform = null;
        }

        private void StartOperation()
        {
            _operationComplete = false;
        }

        private void EndOperation()
        {
            _operationComplete = true;
        }
    }
}