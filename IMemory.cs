﻿namespace Manhattan
{
    public interface IMemory<T>
    {
        public T Read();
        public void Set(T value);
        public void Add(T value);
        public void Sub(T value);
        public void Clear();
    }
}