﻿namespace Manhattan.Math
{
    internal static class AdvancedOperations
    {
        /// <summary>
        /// Sines
        /// </summary>
        internal static double Sin(double x) => System.Math.Sin(x);

        /// <summary>
        /// Cosines   
        /// </summary>
        internal static double Cos(double x) => System.Math.Cos(x);

        /// <summary>
        /// Tangents
        /// </summary>
        internal static double Tan(double x) => System.Math.Tan(x);

        /// <summary>
        /// Cotangets
        /// </summary>
        internal static double Ctg(double x) => System.Math.Cos(x) / System.Math.Sin(x);

        /// <summary>
        /// Logarithm
        /// </summary>
        internal static double Ln(double x) => System.Math.Log(x, System.Math.E);

        /// <summary>
        /// Natural logarithm
        /// </summary>
        internal static double Log10(double x) => System.Math.Log10(x);

        /// <summary>
        /// Modulo
        /// </summary>
        internal static double Mod(double x, double y) => x % y;

        /// <summary>
        /// Pi
        /// </summary>
        internal static double Pi(double x) => x * System.Math.PI;

        /// <summary>
        /// Euler
        /// </summary>
        internal static double E(double x) => x * System.Math.E;

        /// <summary>
        /// Absolute value
        /// </summary>
        internal static double Abs(double x) => System.Math.Abs(x);

        /// <summary>
        /// Factorial
        /// </summary>
        internal static double Fact(int x) => x == 0 ? 1 : x * Fact(x - 1);
    }
}