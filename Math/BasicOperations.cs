﻿namespace Manhattan.Math
{
    internal static class BasicOperations
    {
        /// <summary>
        /// Addition
        /// </summary>
        internal static double Add(double x, double y) => x + y;

        /// <summary>
        /// Subtraction
        /// </summary>
        internal static double Sub(double x, double y) => x - y;

        /// <summary>
        /// Multiplying
        /// </summary>
        internal static double Mul(double x, double y) => x * y;

        /// <summary>
        /// Division
        /// </summary>
        internal static double Div(double x, double y) => x / y;

        /// <summary>
        /// Power
        /// </summary>
        internal static double Pow(double x, double y) => System.Math.Pow(x, y);

        /// <summary>
        /// Square Root
        /// </summary>
        internal static double Sqrt(double x) => System.Math.Sqrt(x);

        /// <summary>
        /// Percentage
        /// </summary>
        internal static double Pct(double x, double y) => x / 100 * y;
    }
}