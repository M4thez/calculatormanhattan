﻿using System;
using System.Globalization;
using System.Windows;
using Manhattan.Conversions;

namespace Manhattan
{
    /// <summary>
    /// Interaction logic for UnitConverter.xaml
    /// </summary>
    public partial class UnitConverter
    {
        private Currency Converter { get; } = new Currency();

        private double Input
        {
            get
            {
                try
                {
                    return double.Parse(TopDisplay.Text, CultureInfo.InvariantCulture);
                }
                catch (FormatException)
                {
                    Display("Invalid input.");
                    return 0;
                }
            }
        }
        
        public UnitConverter()
        {
            InitializeComponent();
            BottomUnits.ItemsSource = TopUnits.ItemsSource = Converter.GetAll();
        }

        private void Btn0_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(0);
        }

        private void Btn1_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(1);
        }

        private void Btn2_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(2);
        }

        private void Btn3_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(3);
        }

        private void Btn4_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(4);
        }

        private void Btn5_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(5);
        }

        private void Btn6_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(6);
        }

        private void Btn7_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(7);
        }

        private void Btn8_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(8);
        }

        private void Btn9_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(9);
        }

        private void BtnPoint_Click(object sender, RoutedEventArgs e)
        {
            if (!TopDisplay.Text.Contains('.')) AddToDisplay(".");
        }

        private void BtnCE_Click(object sender, RoutedEventArgs e)
        {
            ClearDisplay();
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            int length = TopDisplay.Text.Length;
            if (length < 2) ClearDisplay();
            else Display(TopDisplay.Text.Substring(0, length - 1));
        }

        private void Display(double value)
        {
            TopDisplay.Text = value.ToString(CultureInfo.InvariantCulture);
        }

        private void Display(string value)
        {
            TopDisplay.Text = value;
        }

        private void AddToDisplay(int value)
        {
            if (TopDisplay.Text == "0")
            {
                Display(value);
            }
            else AddToDisplay(value.ToString(CultureInfo.InvariantCulture));
        }

        private void AddToDisplay(string value)
        {
            TopDisplay.Text += value;
        }

        private void ClearDisplay()
        {
            TopDisplay.Text = "0";
        }

        private void TopDisplay_SelectionChanged(object sender, RoutedEventArgs e)
        {
            if (TopUnits?.SelectedItem != null && BottomUnits?.SelectedItem != null)
                BottomDisplay.Text = System.Math.Round(Converter.Convert(TopUnits.Text, BottomUnits.Text, Input), 2).ToString(CultureInfo.InvariantCulture);
        }
    }
}
