﻿using System;
using System.Globalization;
using System.Windows;
using Manhattan.Math;

namespace Manhattan
{
    /// <summary>
    /// Interaction logic for AdvancedCalculator.xaml
    /// </summary>
    public partial class AdvancedCalculator
    {
        private delegate double CalcAction(double a, double b);

        private CalcAction _actionToPerform;
        private double _prevInput;
        private bool _operationComplete;

        private double CurrentInput
        {
            get
            {
                try
                {
                    return double.Parse(TxtDisplay.Text, CultureInfo.InvariantCulture);
                }
                catch (FormatException)
                {
                    ResetAll("Invalid input.");
                    return 0;
                }
            }
        }

        public AdvancedCalculator()
        {
            InitializeComponent();
        }

        private void Btn0_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(0);
        }

        private void Btn1_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(1);
        }

        private void Btn2_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(2);
        }

        private void Btn3_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(3);
        }
        
        private void Btn4_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(4);
        }

        private void Btn5_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(5);
        }

        private void Btn6_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(6);
        }

        private void Btn7_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(7);
        }

        private void Btn8_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(8);
        }

        private void Btn9_Click(object sender, RoutedEventArgs e)
        {
            AddToDisplay(9);
        }

        private void BtnSin_Click(object sender, RoutedEventArgs e)
        {
            DisplayPrev($"Sin({CurrentInput}) = ");
            Display(AdvancedOperations.Sin(CurrentInput));
            EndOperation();
        }

        private void BtnCos_Click(object sender, RoutedEventArgs e)
        {
            DisplayPrev($"Cos({CurrentInput}) = ");
            Display(AdvancedOperations.Cos(CurrentInput));
            EndOperation();
        }

        private void BtnTan_Click(object sender, RoutedEventArgs e)
        {
            DisplayPrev($"Tan({CurrentInput}) = ");
            Display(AdvancedOperations.Tan(CurrentInput));
            EndOperation();
        }

        private void BtnCtg_Click(object sender, RoutedEventArgs e)
        {
            DisplayPrev($"Ctg({CurrentInput}) = ");
            Display(AdvancedOperations.Ctg(CurrentInput));
            EndOperation();
        }

        private void BtnLog_Click(object sender, RoutedEventArgs e)
        {
            DisplayPrev($"Log({CurrentInput}) = ");
            Display(AdvancedOperations.Log10(CurrentInput));
            EndOperation();
        }

        private void BtnLn_Click(object sender, RoutedEventArgs e)
        {
            DisplayPrev($"Ln({CurrentInput}) = ");
            Display(AdvancedOperations.Ln(CurrentInput));
            EndOperation();
        }

        private void BtnMod_Click(object sender, RoutedEventArgs e)
        {
            SetOperation(" mod ", AdvancedOperations.Mod);
        }

        private void BtnPi_Click(object sender, RoutedEventArgs e)
        {
            DisplayPrev($"{CurrentInput} * PI = ");
            Display(AdvancedOperations.Pi(CurrentInput));
            EndOperation();
        }

        private void BtnE_Click(object sender, RoutedEventArgs e)
        {
            DisplayPrev($"{CurrentInput} * E = ");
            Display(AdvancedOperations.E(CurrentInput));
            EndOperation();
        }

        private void BtnAbs_Click(object sender, RoutedEventArgs e)
        {
            DisplayPrev($"Abs({CurrentInput}) = ");
            Display(AdvancedOperations.Abs(CurrentInput));
            EndOperation();
        }

        private void BtnFact_Click(object sender, RoutedEventArgs e)
        {
            int val = (int) CurrentInput;
            DisplayPrev($"!{val} = ");
            if (val > 19000) Display("Stack overflow");
            else Display(AdvancedOperations.Fact(val));
            EndOperation();
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            int length = TxtDisplay.Text.Length;
            if (length < 2) ClearDisplay();
            else Display(TxtDisplay.Text.Substring(0, length - 1));
        }

        private void BtnCE_Click(object sender, RoutedEventArgs e)
        {
            ResetOperation();
            ClearDisplay();
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            ResetAll();
        }

        private void BtnNeg_Click(object sender, RoutedEventArgs e)
        {
            Display($"-{TxtDisplay.Text}");
        }

        private void BtnPoint_Click(object sender, RoutedEventArgs e)
        {
            if (!TxtDisplay.Text.Contains('.')) AddToDisplay(".");
        }

        private void BtnEnter_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddToDisplayPrev($"{TxtDisplay.Text} = ");
                if (_actionToPerform != null)
                    Display(_actionToPerform(_prevInput, CurrentInput));
                ResetOperation();
                EndOperation();
            }
            catch (ArithmeticException)
            {
                Display("ERROR!");
            }
        }

        private void Display(double value)
        {
            TxtDisplay.Text = value.ToString(CultureInfo.InvariantCulture);
        }

        private void Display(string value)
        {
            TxtDisplay.Text = value;
        }

        private void DisplayPrev(string value)
        {
            TxtDisplayPrev.Text = value;
        }

        private void AddToDisplay(int value)
        {
            if (_operationComplete)
            {
                Display(value);
                StartOperation();
            }
            else if (TxtDisplay.Text == "0") Display(value);
            else AddToDisplay(value.ToString(CultureInfo.InvariantCulture));
        }

        private void AddToDisplay(string value)
        {
            TxtDisplay.Text += value;
        }

        private void AddToDisplayPrev(string value)
        {
            TxtDisplayPrev.Text += value;
        }

        private void ClearDisplay()
        {
            TxtDisplay.Text = "0";
        }

        private void ClearPreview()
        {
            TxtDisplayPrev.Text = "0";
        }

        private void SavePrevValue()
        {
            try
            {
                _prevInput = double.Parse(TxtDisplay.Text, CultureInfo.InvariantCulture);
                DisplayPrev(TxtDisplay.Text);
                ClearDisplay();
            }
            catch (FormatException)
            {
                ResetAll("Invalid input.");
            }
        }

        private void SetOperation(string sign, CalcAction operation)
        {
            if (_actionToPerform == null) SavePrevValue();
            else TxtDisplayPrev.Text = TxtDisplayPrev.Text.Substring(0, TxtDisplayPrev.Text.Length - 5);
            AddToDisplayPrev(sign);
            _actionToPerform = operation;
        }

        private void ResetAll()
        {
            ResetOperation();
            ClearDisplay();
            ClearPreview();
        }

        private void ResetAll(string message)
        {
            ResetOperation();
            Display(message);
            ClearPreview();
        }

        private void ResetOperation()
        {
            _prevInput = 0;
            _actionToPerform = null;
        }

        private void StartOperation()
        {
            _operationComplete = false;
        }

        private void EndOperation()
        {
            _operationComplete = true;
        }
    }
}