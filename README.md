## Calculator Manhattan - project import from Bosikoz/Manhattan
Desktop calculator application with basic/advanced operations and unit converter made in WPF.

Made by: 
- Mateusz Boboryko - prototyping, whole GUI and app structure;
- Patryk Osiński - team leader (he was the one accepting merge requests), application backend;
- Adam Kozłowski - application backend;
